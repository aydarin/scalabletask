#import "AMViewController.h"

#import "AMRepositoryVM.h"
#import "AMRepositoryCell.h"

@interface AMViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong, readonly) AMRepositoryVM *viewModel;

@end

@implementation AMViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.tableView.dataSource = self;
	self.tableView.delegate = self;

	_viewModel = [[AMRepositoryVM alloc] init];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(updateDataFromViewModel)
												 name:kAMRepositoriesUpdatedNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(repositoryLastCommitUpdated)
												 name:kAMRepositoryLastCommitUpdatedNotification
											   object:nil];


	[self.viewModel reloadModel];
	[self updateDataFromViewModel];
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateDataFromViewModel
{
	self.navigationItem.title = self.viewModel.name;
	[self.tableView reloadData];
}

- (void)repositoryLastCommitUpdated
{
	[self.tableView reloadData];
}

// MARK: UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.viewModel.repositories.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	AMRepositoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kAMRepositoryCellReuseIdentifier];
	if (!cell)
	{
		cell = [[AMRepositoryCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									   reuseIdentifier:kAMRepositoryCellReuseIdentifier];
	}

	AMRepositoryModel *repositoryModel = self.viewModel.repositories[indexPath.row];
	cell.model = repositoryModel;

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
