#import <UIKit/UIKit.h>

#import "AMRepositoryModel.h"

extern NSString * const kAMRepositoryCellReuseIdentifier;

@interface AMRepositoryCell : UITableViewCell

@property (nonatomic, strong, readwrite) AMRepositoryModel *model;

@end
