#import "AMRepositoryCell.h"

NSString * const kAMRepositoryCellReuseIdentifier = @"AMRepositoryCellReuseIdentifier";

@implementation AMRepositoryCell

- (void)setModel:(AMRepositoryModel *)model
{
	_model = model;
	self.textLabel.text = model.name;
	self.detailTextLabel.text = model.lastCommit.sha;
}

@end
