#import "AMRepositoryModel.h"
#import "AMRealmRepositoryModel.h"

static NSString * const kRepositoryNameKey = @"name";

@implementation AMRepositoryModel

- (instancetype)initWithName:(NSString *)name
				  lastCommit:(AMRepositoryCommitModel *)lastCommit
{
	self = [super init];
	if (!self) return nil;

	_name = [name copy];
	_lastCommit = lastCommit;

	return self;
}

+ (instancetype)modelWithRealmObject:(AMRealmRepositoryModel *)realmModel
{
	if (realmModel.name)
	{
		AMRepositoryCommitModel *commit = [AMRepositoryCommitModel modelWithRealmObject:realmModel.lastCommit];
		return [[AMRepositoryModel alloc] initWithName:realmModel.name lastCommit:commit];
	}
	return nil;
}

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary
{
	if (![dictionary isKindOfClass:[NSDictionary class]])
	{
		return nil;
	}

	NSString *repositoryName = dictionary[kRepositoryNameKey];

	if (repositoryName)
	{
		return [[AMRepositoryModel alloc] initWithName:repositoryName
											lastCommit:nil];
	}
	else
	{
		return nil;
	}
}

@end
