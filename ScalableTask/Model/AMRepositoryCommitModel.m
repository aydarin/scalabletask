#import "AMRepositoryCommitModel.h"

static NSString * const kRepositoryCommitSHAKey = @"sha";
static NSString * const kRepositoryCommitCommitKey = @"commit";
static NSString * const kRepositoryCommitAuthorKey = @"author";
static NSString * const kRepositoryCommitDateKey = @"date";

@implementation AMRepositoryCommitModel

- (instancetype)initWithSHA:(NSString *)sha date:(NSDate *)date
{
	self = [super init];
	if (!self) return nil;

	_sha = [sha copy];
	_date = date;

	return self;
}

+ (instancetype)modelWithRealmObject:(AMRealmRepositoryCommitModel *)realmModel
{
	if (realmModel.sha && realmModel.date)
	{
		return [[AMRepositoryCommitModel alloc] initWithSHA:realmModel.sha date:realmModel.date];
	}
	return nil;
}

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary
{
	if (![dictionary isKindOfClass:[NSDictionary class]])
	{
		return nil;
	}

	NSDate *date;
	NSString *sha = dictionary[kRepositoryCommitSHAKey];
	NSDictionary *commitDict = dictionary[kRepositoryCommitCommitKey];
	if ([commitDict isKindOfClass:[NSDictionary class]])
	{
		NSDictionary *authorDict = commitDict[kRepositoryCommitAuthorKey];
		if ([authorDict isKindOfClass:[NSDictionary class]])
		{
			NSString *dateString = authorDict[kRepositoryCommitDateKey];
			date = [[self.class dateFormatter] dateFromString:dateString];
		}
	}

	if (sha && date)
	{
		return [[AMRepositoryCommitModel alloc] initWithSHA:sha date:date];
	}
	else
	{
		return nil;
	}
}

+ (NSDateFormatter *)dateFormatter
{
	static NSDateFormatter *dateFormatter = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
	});
	return dateFormatter;
}

@end
