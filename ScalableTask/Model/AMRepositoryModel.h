#import <Foundation/Foundation.h>

#import "AMRepositoryCommitModel.h"
#import "AMRealmRepositoryModel.h"

@interface AMRepositoryModel : NSObject

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, strong, readwrite) AMRepositoryCommitModel *lastCommit;

- (instancetype)initWithName:(NSString *)name
				  lastCommit:(AMRepositoryCommitModel *)lastCommit NS_DESIGNATED_INITIALIZER;
- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary;
+ (instancetype)modelWithRealmObject:(AMRealmRepositoryModel *)realmModel;

@end
