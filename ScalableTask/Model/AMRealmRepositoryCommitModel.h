#import <Realm/Realm.h>

@interface AMRealmRepositoryCommitModel : RLMObject

@property NSString *sha;
@property NSDate *date;

@end
