#import <Realm/Realm.h>

#import "AMRealmRepositoryCommitModel.h"

@interface AMRealmRepositoryModel : RLMObject

@property NSString *name;
@property AMRealmRepositoryCommitModel *lastCommit;

@end
