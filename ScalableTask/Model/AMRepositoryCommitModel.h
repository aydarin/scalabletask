#import <Foundation/Foundation.h>

#import "AMRealmRepositoryCommitModel.h"

@interface AMRepositoryCommitModel : NSObject

@property (nonatomic, copy, readonly) NSString *sha;
@property (nonatomic, strong, readonly) NSDate *date;

- (instancetype)initWithSHA:(NSString *)sha date:(NSDate *)date NS_DESIGNATED_INITIALIZER;
- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary;
+ (instancetype)modelWithRealmObject:(AMRealmRepositoryCommitModel *)realmModel;

@end
