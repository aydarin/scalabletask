#import <Foundation/Foundation.h>

#import "AMNetworkManager.h"

@interface AMDataProvider : NSObject

+ (instancetype)sharedInstance;

- (void)fetchRepositoriesWithCompletion:(AMLoadingRepositoriesCompletion)completion;
- (void)fetchRepositoryLastCommit:(NSString *)repositoryName
				   withCompletion:(AMLoadingRepositoryLastCommitCompletion)completion;

@end
