#import "AMNetworkManager.h"

static NSString * const kGithubRepositoriesURL = @"https://api.github.com/users/mralexgray/repos";
static NSString * const kGithubRepositoryCommitsURLFormat = @"https://api.github.com/repos/mralexgray/%@/commits";

@implementation AMNetworkManager

+ (instancetype)sharedInstance
{
	static AMNetworkManager *instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[AMNetworkManager alloc] init];
	});
	return instance;
}

- (instancetype)init
{
	self = [super init];
	if (!self) return nil;

	_completionQueue = dispatch_get_main_queue();

	return self;
}

// MARK: Loading

- (void)loadRepositoriesWithCompletion:(AMLoadingRepositoriesCompletion)completion
{
	NSLog(@"load repositories");

	completion = [completion copy];
	NSURL *url = [NSURL URLWithString:kGithubRepositoriesURL];

	__weak typeof(self) wself = self;
	NSURLSessionDataTask *dataTask =
		[[NSURLSession sharedSession] dataTaskWithURL:url
									completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
										typeof(self) self = wself;

										if (error != nil)
										{
											NSLog(@"failed loading repositories");
											dispatch_async(self.completionQueue, ^{
												if (completion) completion(nil, error);
											});
										}
										else
										{
											NSLog(@"completed loading repositories");
											NSError *parseError;
											NSArray<AMRepositoryModel *> *models = [self parseRepositoriesResuts:data
																										   error:&parseError];
											dispatch_async(self.completionQueue, ^{
												if (completion) completion(models, parseError);
											});
										}
									}];
	[dataTask resume];
}

- (void)loadRepositoryLastCommit:(NSString *)repositoryName
				  withCompletion:(AMLoadingRepositoryLastCommitCompletion)completion
{
	NSLog(@"load commits for %@", repositoryName);
	completion = [completion copy];
	NSString *urlString = [NSString stringWithFormat:kGithubRepositoryCommitsURLFormat, repositoryName];
	NSURL *url = [NSURL URLWithString:urlString];

	__weak typeof(self) wself = self;
	NSURLSessionDataTask *dataTask =
		[[NSURLSession sharedSession] dataTaskWithURL:url
									completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
										typeof(self) self = wself;

										if (error != nil)
										{
											NSLog(@"failed loading commits for %@", repositoryName);
											dispatch_async(self.completionQueue, ^{
												if (completion) completion(nil, error);
											});
										}
										else
										{
											NSLog(@"completed loading commits for %@", repositoryName);
											NSError *parseError;
											AMRepositoryCommitModel *lastCommit = [self parseRepositoryCommitsResuts:data
																											   error:&parseError];
											dispatch_async(self.completionQueue, ^{
												if (completion) completion(lastCommit, parseError);
											});
										}
									}];
	[dataTask resume];
}

// MARK: Parsing

- (NSArray<AMRepositoryModel *> *)parseRepositoriesResuts:(NSData *)jsonData error:(NSError **)error
{
	NSArray *repositoryDicts = [NSJSONSerialization JSONObjectWithData:jsonData
															   options:0
																 error:error];
	if (*error)
	{
		return nil;
	}
	else
	{
		NSMutableArray<AMRepositoryModel *> *repositoryModels = [NSMutableArray array];
		for (NSDictionary *repositoryDict in repositoryDicts)
		{
			AMRepositoryModel *model = [AMRepositoryModel modelWithDictionary:repositoryDict];
			if (model)
			{
				[repositoryModels addObject:model];
			}
		}
		return repositoryModels;
	}
}

- (AMRepositoryCommitModel *)parseRepositoryCommitsResuts:(NSData *)jsonData
													error:(NSError **)error
{
	NSArray *commitDicts = [NSJSONSerialization JSONObjectWithData:jsonData
														   options:0
															 error:error];
	if (*error)
	{
		return nil;
	}
	else
	{
		NSMutableArray<AMRepositoryCommitModel *> *commitModels = [NSMutableArray array];
		for (NSDictionary *commitDict in commitDicts)
		{
			AMRepositoryCommitModel *model = [AMRepositoryCommitModel modelWithDictionary:commitDict];
			if (model)
			{
				[commitModels addObject:model];
			}
		}

		AMRepositoryCommitModel *lastCommit;
		for (AMRepositoryCommitModel *commit in commitModels)
		{
			if (!lastCommit || [commit.date timeIntervalSinceDate:lastCommit.date] > 0)
			{
				lastCommit = commit;
			}
		}

		return lastCommit;
	}
}

@end
