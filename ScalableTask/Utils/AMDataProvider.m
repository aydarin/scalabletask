#import "AMDataProvider.h"

#import <Realm/Realm.h>
#import "AMRealmRepositoryModel.h"

@interface AMDataProvider ()

@property (nonatomic, strong, readonly) RLMRealm *realm;

@end

@implementation AMDataProvider

+ (instancetype)sharedInstance
{
	static AMDataProvider *instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[AMDataProvider alloc] init];
	});
	return instance;
}

- (instancetype)init
{
	self = [super init];
	if (!self) return nil;

	_realm = [RLMRealm defaultRealm];

	return self;
}

- (void)fetchRepositoriesWithCompletion:(AMLoadingRepositoriesCompletion)completion
{
	completion = [completion copy];
	RLMResults *results = [AMRealmRepositoryModel allObjects];
	if (results.count > 0)
	{
		if (completion)
		{
			NSMutableArray *resultsArray = [NSMutableArray array];
			for (int i = 0; i < results.count; i++)
			{
				AMRealmRepositoryModel *realmModel = [results objectAtIndex:i];
				AMRepositoryModel *model = [AMRepositoryModel modelWithRealmObject:realmModel];
				[resultsArray addObject:model];
			}
			completion(resultsArray, nil);
		}
	}
	else
	{
		[[AMNetworkManager sharedInstance] loadRepositoriesWithCompletion:^(NSArray<AMRepositoryModel *> *repositories, NSError *error) {

			if (error)
			{
				if (completion) completion(nil, error);
			}
			else
			{
				[self.realm transactionWithBlock:^{
					for (AMRepositoryModel *model in repositories)
					{
						[AMRealmRepositoryModel createInRealm:self.realm
													withValue:@[model.name]];
					}
				}];
				if (completion) completion(repositories, nil);
			}
		}];
	}
}

- (void)fetchRepositoryLastCommit:(NSString *)repositoryName
				   withCompletion:(AMLoadingRepositoryLastCommitCompletion)completion
{
	completion = [completion copy];
	[self fetchRepositoriesWithCompletion:^(NSArray<AMRepositoryModel *> *repositories, NSError *error) {
		if (repositories)
		{
			for (AMRepositoryModel *repository in repositories)
			{
				if ([repository.name isEqualToString:repositoryName])
				{
					if (repository.lastCommit)
					{
						completion(repository.lastCommit, nil);
					}
					else
					{
						[self loadAndSaveRepositoryLastCommit:repositoryName
											   withCompletion:completion];
					}
				}
			}
		}
	}];
}

- (void)loadAndSaveRepositoryLastCommit:(NSString *)repositoryName
						 withCompletion:(AMLoadingRepositoryLastCommitCompletion)completion
{
	[[AMNetworkManager sharedInstance] loadRepositoryLastCommit:repositoryName
												 withCompletion:^(AMRepositoryCommitModel *lastCommit, NSError *error) {
													 if (error)
													 {
														 if (completion) completion(nil, error);
													 }
													 else
													 {
														 [self saveLastCommit:lastCommit
															forRepositoryName:repositoryName];
														 if (completion) completion(lastCommit, nil);
													 }
												 }];
}

- (void)saveLastCommit:(AMRepositoryCommitModel *)lastCommit
	 forRepositoryName:(NSString *)repositoryName
{
	RLMResults *results = [AMRealmRepositoryModel allObjects];

	for (int i = 0; i < results.count; i++)
	{
		AMRealmRepositoryModel *realmModel = [results objectAtIndex:i];
		if ([realmModel.name isEqualToString:repositoryName])
		{
			if (lastCommit)
			{
				[self.realm transactionWithBlock:^{

					AMRealmRepositoryCommitModel *realmCommitModel =
						[AMRealmRepositoryCommitModel createInRealm:self.realm
														  withValue:@[lastCommit.sha, lastCommit.date]];
					realmModel.lastCommit = realmCommitModel;

				}];
			}
		}
	}


}

@end
