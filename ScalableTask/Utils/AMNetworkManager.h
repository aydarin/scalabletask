#import <Foundation/Foundation.h>

#import "AMRepositoryModel.h"
#import "AMRepositoryCommitModel.h"

typedef void(^AMLoadingRepositoriesCompletion)(NSArray<AMRepositoryModel *> *repositories, NSError *error);
typedef void(^AMLoadingRepositoryLastCommitCompletion)(AMRepositoryCommitModel *lastCommit, NSError *error);

@interface AMNetworkManager : NSObject

@property (nonatomic, strong, readonly) dispatch_queue_t completionQueue;

+ (instancetype)sharedInstance;

- (void)loadRepositoriesWithCompletion:(AMLoadingRepositoriesCompletion)completion;
- (void)loadRepositoryLastCommit:(NSString *)repositoryName
				  withCompletion:(AMLoadingRepositoryLastCommitCompletion)completion;

@end
