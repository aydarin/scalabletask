#import "AMRepositoryVM.h"

#import "AMDataProvider.h"

NSString * const kAMRepositoriesUpdatedNotification = @"AMRepositoriesUpdatedNotification";
NSString * const kAMRepositoryLastCommitUpdatedNotification = @"AMRepositoryLastCommitUpdatedNotification";

@interface AMRepositoryVM ()

@property (nonatomic, copy, readwrite) NSArray<AMRepositoryModel *> *repositories;

@end

@implementation AMRepositoryVM

- (instancetype)init
{
	self = [super init];
	if (!self) return nil;

	_name = @"Repositories";

	return self;
}

- (void)reloadModel
{
	__weak typeof(self) wself = self;
	[[AMDataProvider sharedInstance] fetchRepositoriesWithCompletion:^(NSArray<AMRepositoryModel *> *repositories, NSError *error) {
		typeof(self) self = wself;

		if (repositories)
		{
			[self updateRepositories:repositories];
			[self reloadLastCommits];
		}
	}];
}

- (void)updateRepositories:(NSArray<AMRepositoryModel *> *)repositories
{
	self.repositories = repositories;
	[[NSNotificationCenter defaultCenter] postNotificationName:kAMRepositoriesUpdatedNotification
														object:repositories];
}

- (void)reloadLastCommits
{
	for (AMRepositoryModel *repository in self.repositories)
	{
		if (!repository.lastCommit)
		{
			[[AMDataProvider sharedInstance] fetchRepositoryLastCommit:repository.name
														withCompletion:^(AMRepositoryCommitModel *lastCommit, NSError *error) {
															if (!error)
															{
																[self updateRepository:repository
																		withLastCommit:lastCommit];
															}
														}];
		}
	}
}

- (void)updateRepository:(AMRepositoryModel *)repository withLastCommit:(AMRepositoryCommitModel *)lastCommit
{
	repository.lastCommit = lastCommit;
	[[NSNotificationCenter defaultCenter] postNotificationName:kAMRepositoryLastCommitUpdatedNotification
														object:repository];
}

@end
