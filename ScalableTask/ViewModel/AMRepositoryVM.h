#import <Foundation/Foundation.h>

#import "AMRepositoryModel.h"

extern NSString * const kAMRepositoriesUpdatedNotification;
extern NSString * const kAMRepositoryLastCommitUpdatedNotification;

@interface AMRepositoryVM : NSObject

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSArray<AMRepositoryModel *> *repositories;

- (void)reloadModel;

@end
